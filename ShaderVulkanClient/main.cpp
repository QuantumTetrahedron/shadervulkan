
#include <iostream>
#include "Core/Engine.h"

int main() {

	AppInfo info;
	info.title = "Vulkan";
	info.windowWidth = 1600;
	info.windowHeight = 900;
	info.startScene = "testScene";

	Engine engine;

	try {
		engine.Initialize(info);

		engine.MainLoop();

		engine.Cleanup();
	}
	catch (const std::exception& err) {
		std::cout << err.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}