#pragma once

#include "Component.h"
#include "../Utils/IniFile.h"

class RenderComponent : public Component
{
public:
	virtual void Render() = 0;

protected:
	void OnLoad() override;
	void OnRemove() override;
};

class DummyRenderComponent : public RenderComponent {
public:
	void Render() {
		std::cout << "Rendering: " << name << std::endl;
	}

	bool LoadFromFile(const IniFile& file) {
		file.GetValue("name", name, std::string("defaultName"));
		return true;
	}

	std::string name;

	COMPONENT(DummyRenderComponent)
};
