#pragma once
#include "Component.h"

class BehaviourComponent : public Component {
public:
	virtual void Start() = 0;
	virtual void Update(float dt) = 0;
	virtual void OnLeave() = 0;
	virtual void OnMouseDown(int button) {}
	virtual void OnMouseEnter() {}
	virtual void OnMouseLeave() {}
};