#pragma once
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include "Transform.h"
#include "../Utils/IniFile.h"

class Component;
class BehaviourComponent;

class Object : public std::enable_shared_from_this<Object>
{
public:
	void AddComponent(const std::shared_ptr<Component>& c);
	void RemoveComponent(const std::shared_ptr<Component>& c);

	template <typename T>
	std::shared_ptr<T> GetComponent() const;

	template <typename T>
	std::vector<std::shared_ptr<T>> GetAllComponents() const;

	Transform GetWorldTransform() const;
	Transform transform;

	bool IsDead() const;
	void SetParent(const std::shared_ptr<Object>& o);
	bool HasParent() const;
	bool IsActive() const;
	void SetActive(bool active);
	std::string GetName() const;

	void Kill();
	std::shared_ptr<Object> GetParent() const;
	static std::shared_ptr<Object> create();
private:
	Object();

	std::vector<std::shared_ptr<Component>> components;
	std::vector<std::shared_ptr<BehaviourComponent>> behaviours;
	std::weak_ptr<Object> parent;

	bool isDead;
	bool isActive;

	friend class ObjectManager;
	void Start();
	void Update(float dt);
	void OnActive();
	void OnDestroy();
	void RemoveAllComponents();

	friend class ObjectFactory;
	void LoadFromFile(const IniFile& file);

	std::string name;
};

template<typename T>
std::shared_ptr<T> Object::GetComponent() const {
	for (auto c : components)
	{
		std::shared_ptr<T> cast = std::dynamic_pointer_cast<T>(c);
		if (cast)
			return cast;
	}
	return nullptr;
}

template<typename T>
std::vector<std::shared_ptr<T>> Object::GetAllComponents() const {
	std::vector<std::shared_ptr<T>> ret;
	for (auto c : components) {
		std::shared_ptr<T> cast = std::dynamic_pointer_cast<T>(c);
		if (cast)
			ret.push_back(cast);
	}
	return ret;
}
