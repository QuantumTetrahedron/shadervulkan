#include "ObjectManager.h"

PausableVector<std::shared_ptr<Object>> ObjectManager::objects;

void ObjectManager::AddObject(std::shared_ptr<Object> o) {
    objects.push_back(std::move(o));
}

void ObjectManager::Init() {
    objects.reserve(100);
}

void ObjectManager::Update(float dt) {
    for (const auto& o : objects) {
        if (!o->IsDead() && o->IsActive()) {
            o->Update(dt);
        }
    }

    for (auto& o : objects) {
        if (o->isDead) {
            o->OnDestroy();
            o = nullptr;
        }
    }
    objects.erase(std::remove_if(objects.begin(), objects.end(), [](const std::shared_ptr<Object>& obj) {return !obj; }), objects.end());
}

void ObjectManager::Pause() {
    objects.Pause();
}

void ObjectManager::Resume() {
    objects.Resume();
}
void ObjectManager::DestroyAllObjects() {
    objects.MergeAll();
    DestroyObjects();
}

void ObjectManager::DestroyObjects() {
    for (auto o : objects) {
        o->OnDestroy();
    }
    objects.clear();
}

std::shared_ptr<Object> ObjectManager::GetObject(const std::string& name) {
    for (const auto& o : objects) {
        if (o->GetName() == name) {
            return o;
        }
    }
    return nullptr;
}

void ObjectManager::Start() {
    for (auto& o : objects) {
        o->OnActive();
        o->Start();
    }
}

std::vector<std::shared_ptr<Object>> ObjectManager::GetChildrenOf(const Object& obj) {
    return GetChildrenOf(obj.GetName());
}

std::vector<std::shared_ptr<Object>> ObjectManager::GetChildrenOf(const std::string& objName) {
    std::vector<std::shared_ptr<Object>> ret;
    for (auto& o : objects) {
        if (o->GetParent()->GetName() == objName) {
            ret.push_back(o);
        }
    }
    return ret;
}