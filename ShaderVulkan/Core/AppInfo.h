#pragma once
#include <string>

class AppInfo {
public:
	std::string title;
	std::string startScene;
	unsigned int windowWidth = 800;
	unsigned int windowHeight = 600;
};