#pragma once
#include "../InOut/Window.h"
#include "../Gfx/Instance.h"
#include "../Gfx/Surface.h"
#include "../Gfx/Renderer.h"
#include <memory>
#include "AppInfo.h"

class Engine {
public:
	void Initialize(const AppInfo& info);
	void MainLoop();
	void Cleanup();

	static std::shared_ptr<Renderer> getRenderer();
private:
	void Start();

	Window window;
	Instance instance;
	Surface surface;

	static std::shared_ptr<Renderer> renderer;

	std::vector<const char*> getRequiredExtensions();

	float deltaTime, lastFrameTime;
#ifdef NDEBUG
	const bool debug = false;
#else
	const bool debug = true;
#endif
};