#pragma once
#include <string>

class SceneManager
{
public:
	static void RequestLoadScene(const std::string& sceneName);
	static void RequestPauseLoadScene(const std::string& sceneName);
	static void RequestUnpauseScene();
	static void RequestCleanupAll();

	static bool IsSceneChanging();

private:
	friend class Engine;

	static void Init();
	static void Update(float dt);
	static void ProcessRequest();

	enum class Request {
		Load, PauseAndLoad, Resume, Cleanup, None
	};
	static Request request;
	static std::string sceneToLoad;

	static void SetStartingScene(const std::string& sceneName);
	static void Start();
	static void CleanupAll();
	static void LoadScene(const std::string& sceneName);
	static void PauseLoadScene(const std::string& sceneName);
	static void ResumeScene();

	static void LoadObjects(const std::string& sceneName);
};
