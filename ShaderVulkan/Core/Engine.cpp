#include "Engine.h"
#include "../Gfx/Device.h"
#include "SceneManager.h"

std::shared_ptr<Renderer> Engine::renderer;

void Engine::Initialize(const AppInfo& info)
{
	window.create(info.windowWidth, info.windowHeight, info.title.c_str());

	auto extensions = getRequiredExtensions();
	instance.create(extensions, debug);
	surface.create(instance.get(), window);

	auto s = surface.get();
	Device::create(instance.get(), s);

	renderer = std::make_shared<DefaultRenderer>();

	SceneManager::Init();
	SceneManager::SetStartingScene(info.startScene);
}

void Engine::MainLoop()
{
	Start();

	while (!window.shouldClose()) {
		auto currentFrameTime = (float)glfwGetTime();
		deltaTime = currentFrameTime - lastFrameTime;
		lastFrameTime = currentFrameTime;

		glfwPollEvents();

		// TODO: handle window resize

		SceneManager::Update(deltaTime);
		renderer->Render();
		
		SceneManager::ProcessRequest();
	}
}

void Engine::Cleanup()
{
	SceneManager::CleanupAll();
	renderer->Cleanup();
	Device::cleanup();
	surface.cleanup(instance.get());
	instance.cleanup();
	window.cleanup();
}

void Engine::Start()
{
	renderer->Initialize(surface);
	SceneManager::Start();
}

std::vector<const char*> Engine::getRequiredExtensions()
{
	uint32_t glfwExtensionsCount = 0;
	const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionsCount);
	std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionsCount);

	if (debug) {
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}

	return extensions;
}

std::shared_ptr<Renderer> Engine::getRenderer() {
	return renderer;
}