#include "SceneManager.h"
#include <stack>
#include <fstream>
#include <sstream>
#include "../Utils/IniFile.h"
#include "../Gfx/Renderer.h"
#include "ObjectManager.h"
#include "ObjectFactory.h"
#include "Engine.h"

SceneManager::Request SceneManager::request = SceneManager::Request::None;
std::string SceneManager::sceneToLoad;

void SceneManager::LoadScene(const std::string& sceneName) {
    ObjectManager::DestroyAllObjects();
    LoadObjects(sceneName);
}

void SceneManager::PauseLoadScene(const std::string& sceneName) {
    ObjectManager::Pause();
    Engine::getRenderer()->Pause();
    LoadObjects(sceneName);
}

void SceneManager::ResumeScene() {
    ObjectManager::DestroyObjects();
    ObjectManager::Resume();
    Engine::getRenderer()->Resume();
}

void SceneManager::Update(float dt) {
    ObjectManager::Update(dt);
}

void SceneManager::ProcessRequest() {
    if (request == Request::Load)
        LoadScene(sceneToLoad);
    else if (request == Request::PauseAndLoad)
        PauseLoadScene(sceneToLoad);
    else if (request == Request::Resume)
        ResumeScene();
    else if (request == Request::Cleanup)
        CleanupAll();
    request = Request::None;
}

void SceneManager::SetStartingScene(const std::string& sceneName) {
    sceneToLoad = sceneName;
}

void SceneManager::CleanupAll() {
    Engine::getRenderer()->ClearComponents();
    ObjectManager::DestroyAllObjects();
}

void SceneManager::RequestLoadScene(const std::string& sceneName) {
    request = Request::Load;
    sceneToLoad = sceneName;
}

void SceneManager::RequestPauseLoadScene(const std::string& sceneName) {
    request = Request::PauseAndLoad;
    sceneToLoad = sceneName;
}

void SceneManager::RequestUnpauseScene() {
    request = Request::Resume;
}

void SceneManager::RequestCleanupAll() {
    request = Request::Cleanup;
}

void SceneManager::LoadObjects(const std::string& sceneName) {
    std::stack<std::string> parentingStack;
    std::string filePath = "Scenes/" + sceneName + "/" + sceneName + ".lvl";
    std::ifstream file(filePath.c_str(), std::ios::in);

    std::string line;
    int lastLevel = 1;
    std::string lastName;

    while (getline(file, line)) {
        if (line.empty() || line[0] == ';') continue;

        std::stringstream line_ss(line);
        std::string levelStr, name, archetype;

        line_ss >> levelStr >> name >> archetype;
        int level = levelStr.size();

        if (level == lastLevel + 1) {
            parentingStack.push(lastName);
            lastLevel = level;
        }
        else if (level < lastLevel) {
            while (level < lastLevel) {
                parentingStack.pop();
                lastLevel--;
            }
        }
        else if (level > lastLevel + 1) {
            throw std::runtime_error("Wrong level hierarchy");
        }

        lastName = name;

        if (archetype.empty()) {
            ObjectFactory::RemoveArchetype();
        }
        else {
            std::string archetypeFilePath = "Archetypes/" + archetype + ".ini";
            IniFile archetypeFile;
            archetypeFile.ReadFile(archetypeFilePath.c_str());
            ObjectFactory::SetArchetype(archetypeFile);
        }

        std::string objectFilePath = "Scenes/";
        objectFilePath += sceneName;
        objectFilePath += "/";
        objectFilePath += name;
        objectFilePath += ".ini";
        IniFile objectFile;
        objectFile.ReadFile(objectFilePath.c_str());
        auto o = ObjectFactory::Create(objectFile, name);
        if (o) {
            ObjectManager::AddObject(o);
            if (!parentingStack.empty()) {
                o->SetParent(ObjectManager::GetObject(parentingStack.top()));
            }
        }
    }
    ObjectManager::Start();
}

void SceneManager::Init() {
    ObjectManager::Init();
}

bool SceneManager::IsSceneChanging() {
    return request != Request::None;
}

void SceneManager::Start() {
    ObjectManager::DestroyAllObjects();
    LoadObjects(sceneToLoad);
}