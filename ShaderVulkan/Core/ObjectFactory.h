#pragma once

#include "../Utils/IniFile.h"
#include "Object.h"

class ObjectFactory
{
public:
	static void SetArchetype(const IniFile& archetypeFile);
	static void RemoveArchetype();
	static std::shared_ptr<Object> Create(IniFile& objectFile, const std::string& objectName);

private:
	static bool useArchetype;
	static IniFile currArchetype;
};

