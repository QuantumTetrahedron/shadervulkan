#include "RenderComponent.h"
#include "Engine.h"

void RenderComponent::OnLoad() {
    Engine::getRenderer()->RegisterComponent(std::dynamic_pointer_cast<RenderComponent>(shared_from_this()));
}

void RenderComponent::OnRemove() {
    Engine::getRenderer()->UnregisterComponent(std::dynamic_pointer_cast<RenderComponent>(shared_from_this()));
}