#include "ComponentFactory.h"
#include <iostream>

std::shared_ptr<ComponentFactory::mapType> ComponentFactory::map;

std::shared_ptr<Component> ComponentFactory::create(const std::string& className) {
    auto it = getMap()->find(className);
    if (it == getMap()->end()) {
        std::cerr << "Component type " << className << " not registered" << std::endl;
        return nullptr;
    }
    else {
        return it->second();
    }
}

std::shared_ptr<ComponentFactory::mapType> ComponentFactory::getMap() noexcept {
    if (!map) {
        map = std::make_shared<ComponentFactory::mapType>();
    }
    return map;
}