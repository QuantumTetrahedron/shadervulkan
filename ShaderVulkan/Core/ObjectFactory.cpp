#include "ObjectFactory.h"
#include "Component.h"
#include "ComponentFactory.h"

IniFile ObjectFactory::currArchetype;
bool ObjectFactory::useArchetype = false;

std::shared_ptr<Object> ObjectFactory::Create(IniFile& objectFile, const std::string& objectName) {
    auto newObject = Object::create();

    newObject->name = objectName;

    IniFile file;
    if (useArchetype) {
        file = currArchetype.UpdateWith(objectFile);
    }
    else {
        file = objectFile;
    }

    file.SetToSection("");
    newObject->LoadFromFile(file);

    std::string components;
    file.GetValue("components", components);
    std::stringstream ss(components);

    std::string compName;
    while (ss >> compName) {
        std::shared_ptr<Component> comp = ComponentFactory::create(compName);
        file.SetToSection(compName);
        if (!comp->LoadFromFile(file)) {
            return nullptr;
        }
        newObject->AddComponent(comp);
    }

    for (const auto& c : newObject->GetAllComponents<Component>()) {
        c->OnLoad();
    }

    return newObject;
}

void ObjectFactory::SetArchetype(const IniFile& archetypeFile) {
    currArchetype = archetypeFile;
    useArchetype = true;
}

void ObjectFactory::RemoveArchetype() {
    useArchetype = false;
}
