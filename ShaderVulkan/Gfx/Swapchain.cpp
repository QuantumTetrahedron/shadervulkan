#include "Swapchain.h"
#include "Device.h"

void Swapchain::create(const Surface& surface)
{
	createInternal(surface);
}

void Swapchain::cleanup()
{
	cleanupInternal();
}

vk::SurfaceFormatKHR Swapchain::chooseSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats) const
{
	for (const auto& availableFormat : availableFormats) {
		if (availableFormat.format == vk::Format::eB8G8R8A8Srgb && availableFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) {
			return availableFormat;
		}
	}
	return availableFormats[0];
}

vk::PresentModeKHR Swapchain::choosePresentMode(const std::vector<vk::PresentModeKHR>& availableModes) const
{
	for (const auto& availableMode : availableModes) {
		if (availableMode == vk::PresentModeKHR::eMailbox) {
			return availableMode;
		}
	}
	return vk::PresentModeKHR::eFifo;
}

vk::Extent2D Swapchain::chooseExtent(const vk::SurfaceCapabilitiesKHR& capabilities, uint32_t w, uint32_t h)
{
	if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
		return capabilities.currentExtent;
	}
	else {
		vk::Extent2D actualExtent = { w, h };
		actualExtent.width = std::clamp(actualExtent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
		actualExtent.height = std::clamp(actualExtent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);
		return actualExtent;
	}
}

void Swapchain::createInternal(const Surface& surface)
{
	SwapchainSupportDetails swapchainSupport = Device::getPhysicalDevice().getSwapchainSupport();

	vk::SurfaceFormatKHR surfaceFormat = chooseSurfaceFormat(swapchainSupport.formats);
	vk::PresentModeKHR presentMode = choosePresentMode(swapchainSupport.presentModes);
	vk::Extent2D imageExtent = chooseExtent(swapchainSupport.capabilities, surface.width, surface.height);

	uint32_t imageCount = swapchainSupport.capabilities.minImageCount + 1;
	if (swapchainSupport.capabilities.maxImageCount > 0 && imageCount > swapchainSupport.capabilities.maxImageCount) {
		imageCount = swapchainSupport.capabilities.maxImageCount;
	}

	vk::SwapchainCreateInfoKHR swapchainInfo(vk::SwapchainCreateFlagsKHR(),
		surface.get(),
		imageCount,
		surfaceFormat.format,
		surfaceFormat.colorSpace,
		imageExtent,
		1,
		vk::ImageUsageFlagBits::eColorAttachment,
		vk::SharingMode::eExclusive,
		{},
		swapchainSupport.capabilities.currentTransform,
		vk::CompositeAlphaFlagBitsKHR::eOpaque,
		presentMode,
		true,
		nullptr);

	QueueFamilyIndices indices = Device::getPhysicalDevice().getQueueFamilies();
	uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };
	
	if (indices.graphicsFamily.value() != indices.presentFamily.value()) {
		swapchainInfo.imageSharingMode = vk::SharingMode::eConcurrent;
		swapchainInfo.queueFamilyIndexCount = 2;
		swapchainInfo.pQueueFamilyIndices = queueFamilyIndices;
	}

	swapchain = Device::get().createSwapchainKHR(swapchainInfo);
	imageFormat = surfaceFormat.format;
	extent = imageExtent;

	images = Device::get().getSwapchainImagesKHR(swapchain);

	createImageViews();
}

void Swapchain::cleanupInternal()
{
	for (auto imageView : imageViews) {
		Device::get().destroyImageView(imageView);
	}
	Device::get().destroySwapchainKHR(swapchain);
}

void Swapchain::createImageViews()
{
	imageViews.resize(images.size());

	for (int i = 0; i < images.size(); ++i) {
		vk::ImageViewCreateInfo createInfo{};

		createInfo.image = images[i];
		createInfo.viewType = vk::ImageViewType::e2D;
		createInfo.format = imageFormat;

		createInfo.components.r = vk::ComponentSwizzle::eIdentity;
		createInfo.components.g = vk::ComponentSwizzle::eIdentity;
		createInfo.components.b = vk::ComponentSwizzle::eIdentity;
		createInfo.components.a = vk::ComponentSwizzle::eIdentity;

		createInfo.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;

		imageViews[i] = Device::get().createImageView(createInfo);
	}
}
