#pragma once
#include <stdexcept>
#include <iostream>
#include <vector>
#include "vk.h"

class Debug
{
public:
    void setup(const vk::Instance& instance, const vk::DispatchLoaderDynamic& dispatchLoader);
    void cleanup(const vk::Instance& instance, const vk::DispatchLoaderDynamic& dispatchLoader);

    bool checkValidationLayerSupport();
    vk::DebugUtilsMessengerCreateInfoEXT getMessengerCreateInfo();

    const std::vector<const char*> validationLayers = {
        "VK_LAYER_KHRONOS_validation"
    };
private:
    vk::DebugUtilsMessengerEXT debugMessenger;
};

