#pragma once
#include "Instance.h"
#include "PhysicalDevice.h"

class Device
{
public:
	static void create(const vk::Instance& instance, vk::SurfaceKHR& surface);
	static void cleanup();

	static vk::Device get();
	static vk::Queue getGraphicsQueue();
	static PhysicalDevice getPhysicalDevice();

private:
	static vk::Device device;
	static PhysicalDevice physicalDevice;

	static vk::Queue graphicsQueue;
	static vk::Queue presentQueue;
};

