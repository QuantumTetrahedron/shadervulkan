#include "Device.h"
#include <set>

vk::Device Device::device;
PhysicalDevice Device::physicalDevice;
vk::Queue Device::graphicsQueue;
vk::Queue Device::presentQueue;

void Device::create(const vk::Instance& instance, vk::SurfaceKHR& surface)
{
	physicalDevice.pick(instance, surface);

	QueueFamilyIndices indices = physicalDevice.getQueueFamilies();
	
	std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };
	std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;

	float queuePriority = 1.0f;

	for (uint32_t queueFamily : uniqueQueueFamilies) {
		vk::DeviceQueueCreateInfo queueCreateInfo{};
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	vk::PhysicalDeviceFeatures deviceFeatures{};

	vk::DeviceCreateInfo createInfo{};
	createInfo
		.setQueueCreateInfos(queueCreateInfos)
		.setPEnabledFeatures(&deviceFeatures)
		.setPEnabledExtensionNames(physicalDevice.deviceExtensions);

	device = physicalDevice.get().createDevice(createInfo);

	graphicsQueue = device.getQueue(indices.graphicsFamily.value(), 0);
	presentQueue = device.getQueue(indices.presentFamily.value(), 0);
}

void Device::cleanup()
{
	device.destroy();
}

vk::Device Device::get()
{
	return device;
}

vk::Queue Device::getGraphicsQueue()
{
	return graphicsQueue;
}

PhysicalDevice Device::getPhysicalDevice()
{
	return physicalDevice;
}
