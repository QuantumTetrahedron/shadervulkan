#include "Debug.h"

VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData)
{
    std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;
    return VK_FALSE;
}

void Debug::setup(const vk::Instance& instance, const vk::DispatchLoaderDynamic& dispatchLoader)
{
	vk::DebugUtilsMessengerCreateInfoEXT createInfo = getMessengerCreateInfo();
	debugMessenger = instance.createDebugUtilsMessengerEXT(createInfo, nullptr, dispatchLoader);
}

void Debug::cleanup(const vk::Instance& instance, const vk::DispatchLoaderDynamic& dispatchLoader)
{
    instance.destroyDebugUtilsMessengerEXT(debugMessenger, nullptr, dispatchLoader);
}

bool Debug::checkValidationLayerSupport()
{
    std::vector<vk::LayerProperties> availableLayers = vk::enumerateInstanceLayerProperties();

    std::cout << "Required layers" << std::endl;
    for (const char* layer : validationLayers) {
        std::cout << layer << std::endl;
    }

    std::cout << "Available layers" << std::endl;
    for (vk::LayerProperties& layer : availableLayers) {
        std::cout << layer.layerName << std::endl;
    }

    for (const char* layerName : validationLayers) {
        bool layerFound = false;
        for (const auto& layerProperties : availableLayers) {
            if (strcmp(layerName, layerProperties.layerName) == 0) {
                layerFound = true;
                break;
            }
        }
        if (!layerFound) return false;
    }
    return true;
}

vk::DebugUtilsMessengerCreateInfoEXT Debug::getMessengerCreateInfo()
{
	return { {},
		vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning | vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
		vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance |
			vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation,
		debugCallback };
}

