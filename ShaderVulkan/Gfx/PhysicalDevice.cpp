#include "PhysicalDevice.h"
#include <set>

void PhysicalDevice::pick(const vk::Instance& instance, vk::SurfaceKHR& surface)
{
	std::vector<vk::PhysicalDevice> physicalDevices = instance.enumeratePhysicalDevices();

	if (physicalDevices.empty()) {
		throw std::runtime_error("failed to find a GPU with Vulkan support");
	}

	std::cout << "Available devices" << std::endl;
	for (const auto& device : physicalDevices) {
		std::cout << device.getProperties().deviceName << std::endl;
	}

	auto iterator = std::find_if(physicalDevices.begin(), physicalDevices.end(), [&](vk::PhysicalDevice& device) {
		return isDeviceSuitable(device, surface);
	});

	if (iterator == physicalDevices.end()) {
		throw std::runtime_error("failed to find a suitable GPU");
	}

	physicalDevice = *iterator;
	queueFamilies = findQueueFamilies(physicalDevice, surface);
	swapchainSupport = querySwapchainSupport(physicalDevice, surface);

	std::cout << "Picked device: " << physicalDevice.getProperties().deviceName << std::endl;
}

vk::PhysicalDevice PhysicalDevice::get()
{
	return physicalDevice;
}

QueueFamilyIndices PhysicalDevice::getQueueFamilies() const
{
	return queueFamilies;
}

SwapchainSupportDetails PhysicalDevice::getSwapchainSupport() const
{
	return swapchainSupport;
}

bool PhysicalDevice::isDeviceSuitable(vk::PhysicalDevice device, vk::SurfaceKHR& surface) const
{
	QueueFamilyIndices indices = findQueueFamilies(device, surface);

	bool extensionSupport = checkDeviceExtionSupport(device);

	bool swapchainAdequate = false;
	if (extensionSupport) {
		SwapchainSupportDetails swapchainSupport = querySwapchainSupport(device, surface);
		swapchainAdequate = !swapchainSupport.formats.empty() && !swapchainSupport.presentModes.empty();
	}
	
	return indices.isComplete() && extensionSupport && swapchainAdequate;
}

bool PhysicalDevice::checkDeviceExtionSupport(vk::PhysicalDevice device) const
{
	std::vector<vk::ExtensionProperties> availableExtensions = device.enumerateDeviceExtensionProperties();

	std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());
	for (const auto& extensions : availableExtensions) {
		requiredExtensions.erase(extensions.extensionName);
	}

	return requiredExtensions.empty();
}

QueueFamilyIndices PhysicalDevice::findQueueFamilies(vk::PhysicalDevice device, const vk::SurfaceKHR& surface) const
{
	QueueFamilyIndices indices;
	std::vector<vk::QueueFamilyProperties> queueFamilies = device.getQueueFamilyProperties();

	std::cout << "Device queue families" << std::endl;
	for (int i = 0; i < queueFamilies.size(); ++i) {
		vk::QueueFamilyProperties family = queueFamilies[i];
		std::cout << "Family " << i << ": ";
		std::cout << ((family.queueFlags & vk::QueueFlagBits::eGraphics) ? "graphics, " : "");
		std::cout << ((family.queueFlags & vk::QueueFlagBits::eCompute) ? "compute, " : "");
		std::cout << ((family.queueFlags & vk::QueueFlagBits::eProtected) ? "protected, " : "");
		std::cout << ((family.queueFlags & vk::QueueFlagBits::eSparseBinding) ? "sparse binding, " : "");
		std::cout << ((family.queueFlags & vk::QueueFlagBits::eTransfer) ? "transfer, " : "");
		std::cout << " -- count: " << family.queueCount << std::endl;
	}

	for (int i = 0; i < queueFamilies.size(); ++i) {
		vk::QueueFamilyProperties family = queueFamilies[i];

		if (family.queueFlags & vk::QueueFlagBits::eGraphics) {
			indices.graphicsFamily = i;
		}

		if (device.getSurfaceSupportKHR(i, surface)) {
			indices.presentFamily = i;
		}

		if (indices.isComplete()) {
			break;
		}
	}

	return indices;
}

SwapchainSupportDetails PhysicalDevice::querySwapchainSupport(vk::PhysicalDevice device, const vk::SurfaceKHR& surface) const
{
	SwapchainSupportDetails details;

	details.capabilities = device.getSurfaceCapabilitiesKHR(surface);
	details.formats = device.getSurfaceFormatsKHR(surface);
	details.presentModes = device.getSurfacePresentModesKHR(surface);

	return details;
}
