#include "Renderer.h"

void Renderer::RegisterComponent(std::shared_ptr<RenderComponent> rc) {
    renderComponents.push_back(std::move(rc));
}

void Renderer::UnregisterComponent(const std::shared_ptr<RenderComponent>& rc) {
    renderComponents.erase(std::remove_if(renderComponents.begin(), renderComponents.end(), [rc](auto r) {return r == rc; }), renderComponents.end());
}

void Renderer::Pause()
{
    renderComponents.Pause();
}

void Renderer::Resume()
{
    renderComponents.Resume();
}

void Renderer::ClearComponents()
{
    renderComponents.ClearAll();
}