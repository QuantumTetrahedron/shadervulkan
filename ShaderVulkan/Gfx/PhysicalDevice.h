#pragma once
#include "Instance.h"
#include <optional>

struct QueueFamilyIndices 
{
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentFamily;

	bool isComplete() {
		return graphicsFamily.has_value() && presentFamily.has_value();
	}
};

struct SwapchainSupportDetails {
	vk::SurfaceCapabilitiesKHR capabilities;
	std::vector<vk::SurfaceFormatKHR> formats;
	std::vector<vk::PresentModeKHR> presentModes;
};

class PhysicalDevice
{
public:
	void pick(const vk::Instance& instance, vk::SurfaceKHR& surface);

	vk::PhysicalDevice get();
	QueueFamilyIndices getQueueFamilies() const;
	SwapchainSupportDetails getSwapchainSupport() const;

	const std::vector<const char*> deviceExtensions = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

private:
	vk::PhysicalDevice physicalDevice;
	QueueFamilyIndices queueFamilies;
	SwapchainSupportDetails swapchainSupport;

	bool isDeviceSuitable(vk::PhysicalDevice device, vk::SurfaceKHR& surface) const;
	bool checkDeviceExtionSupport(vk::PhysicalDevice device) const;
	QueueFamilyIndices findQueueFamilies(vk::PhysicalDevice device, const vk::SurfaceKHR& surface) const;
	SwapchainSupportDetails querySwapchainSupport(vk::PhysicalDevice device, const vk::SurfaceKHR& surface) const;
};

