#pragma once

#include "vk.h"
#include <vector>
#include "Debug.h"

class Instance {
public:
	void create(const std::vector<const char*>& extensions, bool debug);
	void cleanup();

	vk::Instance get();
private:
	vk::Instance _instance;
	Debug debugManager;
	bool debugEnabled;

	vk::DispatchLoaderDynamic dispatchLoader;
	void initDispatchLoader();
};