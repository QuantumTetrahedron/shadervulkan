#pragma once
#include "vk.h"
#include "../InOut/Window.h"

class Surface
{
public:
	void create(const vk::Instance& instance, const Window& window);
	void cleanup(const vk::Instance& instance);

	vk::SurfaceKHR get() const;

	uint32_t width, height;
private:
	vk::SurfaceKHR surface;
};

