#include "Surface.h"

void Surface::create(const vk::Instance& instance, const Window& window)
{
	surface = window.createSurface(instance);
	width = window.pixelWidth;
	height = window.pixelHeight;
}

void Surface::cleanup(const vk::Instance& instance)
{
	instance.destroySurfaceKHR(surface);
}

vk::SurfaceKHR Surface::get() const
{
	return surface;
}
