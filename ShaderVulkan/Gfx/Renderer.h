#pragma once
#include "../Core/RenderComponent.h"
#include "Swapchain.h"
#include "../Utils/PausableVector.h"

class Renderer {
public:
	virtual void Initialize(const Surface& surface) = 0;
	virtual void Render() = 0;
	virtual void Cleanup() = 0;

	void RegisterComponent(std::shared_ptr<RenderComponent> rc);
	void UnregisterComponent(const std::shared_ptr<RenderComponent>& rc);

	void Pause();
	void Resume();
	void ClearComponents();
protected:
	PausableVector<std::shared_ptr<RenderComponent>> renderComponents;
};

class DefaultRenderer : public Renderer {
public:
	void Initialize(const Surface& surface) {
		swapchain.create(surface);
	}

	void Render() {
		for (auto& component : renderComponents) {
			component->Render();
		}
	}

	void Cleanup() {
		swapchain.cleanup();
	}

protected:
	Swapchain swapchain;
};