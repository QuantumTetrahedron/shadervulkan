#include "Instance.h"

void Instance::create(const std::vector<const char*>& extensions, bool debug)
{
	debugEnabled = debug;
	std::vector<const char*> layers;
	vk::DebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo;
	void* pNext = nullptr;

	if (debugEnabled) {
		if (!debugManager.checkValidationLayerSupport()) {
			throw std::runtime_error("validation layers requested, but not available");
		}
		layers = debugManager.validationLayers;
		debugMessengerCreateInfo = debugManager.getMessengerCreateInfo();
		pNext = &debugMessengerCreateInfo;
	}

	vk::ApplicationInfo applicationInfo(
		"ShaderVulkanClient", 1,
		"Engie McEngineFace", 1,
		VK_API_VERSION_1_2
	);

	std::cout << "Required extensions" << std::endl;
	for (auto& ext : extensions) {
		std::cout << ext << std::endl;
	}

	std::vector<vk::ExtensionProperties> availableExtensions = vk::enumerateInstanceExtensionProperties();
	std::cout << "Available extensions" << std::endl;
	for (auto& extension : availableExtensions) {
		std::cout << extension.extensionName << std::endl;
	}
	
	vk::InstanceCreateInfo instanceCreateInfo(
		{},
		&applicationInfo,
		layers,
		extensions
	);
	instanceCreateInfo.setPNext(pNext);

	_instance = vk::createInstance(instanceCreateInfo);
	initDispatchLoader();

	if (debugEnabled) {
		debugManager.setup(_instance, dispatchLoader);
	}
}

void Instance::cleanup()
{
	if (debugEnabled) {
		debugManager.cleanup(_instance, dispatchLoader);
	}
	_instance.destroy();
}

vk::Instance Instance::get()
{
	return _instance;
}

void Instance::initDispatchLoader()
{
	vk::DynamicLoader dl;
	PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr = dl.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr");
	dispatchLoader.init(_instance, vkGetInstanceProcAddr);
}
