#pragma once
#include "vk.h"
#include "Surface.h"

class Swapchain
{
public:
	void create(const Surface& surface);
	void cleanup();

private:
	vk::SwapchainKHR swapchain;
	vk::Format imageFormat;
	vk::Extent2D extent;
	std::vector<vk::Image> images;
	std::vector<vk::ImageView> imageViews;

	vk::SurfaceFormatKHR chooseSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats) const;
	vk::PresentModeKHR choosePresentMode(const std::vector<vk::PresentModeKHR>& availableModes) const;
	vk::Extent2D chooseExtent(const vk::SurfaceCapabilitiesKHR& capabilities, uint32_t w, uint32_t h);

	void createInternal(const Surface& surface);
	void cleanupInternal();

	void createImageViews();
};

