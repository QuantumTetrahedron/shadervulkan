#include "Window.h"

void Window::create(uint32_t w, uint32_t h, const char* title) 
{
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    _window = glfwCreateWindow(w, h, title, nullptr, nullptr);

    screenWidth = w;
    screenHeight = h;

    int pw, ph;
    glfwGetFramebufferSize(_window, &pw, &ph);
    pixelWidth = static_cast<uint32_t>(pw);
    pixelHeight = static_cast<uint32_t>(ph);
}

void Window::cleanup() 
{
    glfwDestroyWindow(_window);
    glfwTerminate();
}

void Window::close() 
{
    glfwSetWindowShouldClose(_window, true);
}

bool Window::shouldClose() 
{
    return glfwWindowShouldClose(_window);
}

vk::SurfaceKHR Window::createSurface(const vk::Instance& instance) const
{
    VkSurfaceKHR surface;
    if (glfwCreateWindowSurface(instance, _window, nullptr, &surface) != VK_SUCCESS) {
        throw std::runtime_error("failed to create window surface");
    }
    return vk::SurfaceKHR(surface);
}
