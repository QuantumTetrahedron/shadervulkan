#pragma once

#include "../Gfx/vk.h"

class Window {
public:
	void create(uint32_t w, uint32_t h, const char* title);
	void cleanup();

	void close();
	bool shouldClose();

	vk::SurfaceKHR createSurface(const vk::Instance& instance) const;

	uint32_t screenWidth, screenHeight;
	uint32_t pixelWidth, pixelHeight;
private:
	GLFWwindow* _window;
};
